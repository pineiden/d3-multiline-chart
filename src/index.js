const axios = require('axios');
const _ = require('lodash');
import {ExcavationProgress} from './chart';
const d3 = require("d3");

console.log("Object", ExcavationProgress);

let ep_chart = ExcavationProgress("#chart", {});

import './estilos.css';

console.log("Chart object", ep_chart);

const params = JSON.parse(document.getElementById("parameters").textContent);
ep_chart.init(params);

console.log("parameters", params);

let token = "";
axios.post(params.auth_login, params.auth )
.then( (response)=>{
    console.log("Ok login",response);
    token = response.data.token;
    console.log("Token", token)
    let dataset=get_data("02", token);
    build_chart(dataset);
}
)
.catch((error)=>{
console.log("error", error);
})


function get_data(axis, token){
    let dates = '';
    if (params.start_date!=='undefined'){
      dates = `&desde=${params.start_date}`;
    }
    if (params.end_date!=='undefined'){
      dates = `${dates}&hasta=${params.end_date}`;
    };  
    const query = `${params.url}?axis=${axis}${dates}`;
    let dataset = {};
    let auth = {Authorization:`token ${token}`};
    axios.get(query, {headers:auth})
    .then((response)=>{
        let data = response.data.results;      
        // dataset = _(data)
        //     .groupBy((elem)=>{return elem.phase;})
        //     .value();
        ep_chart.update({dataset:data});
    })
    .catch((error)=>{
        console.log("Error on query", query, error);
    })
    return dataset;
}

function build_chart(data){}
