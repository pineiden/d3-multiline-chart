const d3 = require("d3");

export default function ExcavationProgress(parentSelector, options){
   const opts = {
       width: 900,
       height: 250,
       class_name: 'excavation_progress',
       legend: 'Progreso de Excavación',
       show_legend:true,
       margin: {left:35,right:10,bottom:20,top:20}
   };
  
  let chart = Object;
  let component = undefined;

  if(opts.hasOwnProperty(component)){
    component = opts.component;
  }

  if (!component) {
    component = d3
      .select(parentSelector)
      .append('svg')
      .attr('class', 'svg-class')
      .attr('width', opts.width+"px")
      .attr('height', opts.height+"px");
      //.on("mousemove touchmove", moved);
    chart = component;
  } else {
    chart = component.append('g').attr('class', opts.class_name);
  };

  chart.dateformat = d3.timeFormat("%d-%m-%Y");

  // main attributes
  component
    .append("g")
    .attr("class",opts.class_name);


  // refresh chart

  chart.refresh  = (new_options) => {
    if (typeof new_options !== 'undefined'){
     Object.keys(new_options).forEach((e)=>{
       if (typeof new_options[e] !=='undefined'){
         opts[e] = new_options[e];
       }
     }); 
    }
  };

  chart.refresh(options);

  chart.tooltip = d3.select(parentSelector)
  .append("div")
  .attr("id","tooltip")
  .style("position", "absolute")
  .style("visibility", "hidden")
  .text("Posición:")

  
  chart.refreshAxis = (data) => {
    let dates = [new Date(opts.start_date), new Date(opts.end_date)];
    console.log("opts dates", dates);
    
    if (data.dataset){      
      let max_value=data.dataset.map((item)=>{
        return item.excavated_distance;
      }).reduce((a,b)=>{
        if (b>a){return b;}
        else {return a;}
      });
      //if (max_value>opts.base_distance){
      opts.base_distance = max_value*1.2;

      //dates = d3.extent(data.dataset.map ((item)=>{return new Date(item.datetime);}));
      console.log("new dates", dates);
      //}
      //let values  = group.map((item)=>{return item.excavated_distance});
      //distances[phase] = values;
    };
    const xLimits = [opts.margin.left, opts.width-10*opts.margin.right];
    console.log("set new dates", dates);
    let xAxis = d3.scaleTime()
        .domain(dates)
        .range(xLimits);
    // y axis form 0 -> 100m al inicio
    const yLimits = [opts.margin.top, opts.height-opts.margin.bottom];
    let yAxis = d3.scaleLinear()
        .domain([opts.base_distance,0])
        .range(yLimits);

    chart.xAxis = xAxis;
    chart.yAxis = yAxis;
    
    component
      .selectAll("g.x.axis")
      .attr("transform", `translate(${opts.margin.left},${opts.height - opts.margin.bottom})`)    
      .call(d3.axisBottom(xAxis));
    component
      .selectAll("g.y.axis")
      .attr("transform", `translate(${opts.margin.left*2},0)`)
      .call(d3.axisLeft(yAxis));
  };

  
  chart.init = (data) => {
    chart.refresh(data);
    chart.append("g").attr("class","x axis");
    chart.append("g").attr("class","y axis");    
    chart.refreshAxis(data);
    /*
      build axis with start_date, end_date and base_distance-
     */
    // x scale -> timeformat
    
  };

  chart.update = (newData) => {
    chart.refreshAxis(newData);
    /*
      draw lines and circles for every group
     */
    if (newData.dataset){
      const dataset = d3.group(newData.dataset, item => item.phase);

      const res = [...dataset.keys()];

     
      const color =  d3.scaleOrdinal()
          .domain(res)
          .range(
            [
              '#e41a1c',
              '#377eb8',
              '#4daf4a',
              '#984ea3',
              '#ff7f00',
              '#ffff33',
              '#a65628',
              '#f781bf',
              '#999999']);

      
      /*
        how to use groups-> Map

        https://observablehq.com/@d3/d3-group
        https://www.d3-graph-gallery.com/graph/line_several_group.html
        https://blocks.lsecities.net/d3noob/bdaf9d5abc467a4895fb115330be35b2
       */
      
      

      console.log("dataset", component, dataset);

      
      component.selectAll(".line")
      .data(dataset)
      .join("path")
         .attr("fill","none")
         .attr("stroke", ([key, array])=>{
           return color(key);})
         .attr("stroke-width",1.5)
         .attr("d", ([key, array])=>{
           let line = d3.line()
           .x((item)=>{
             //console.log("x item", chart.xAxis(new Date(item.datetime)));
             return chart.xAxis(new Date(item.datetime));})
           .y((item)=>{
             //console.log("y item", chart.yAxis(item.excavated_distance));
             return chart.yAxis(item.excavated_distance);})
           (array);
           return line;
         });


      var circles = {};

      var mousemove = function(d,item){
        let date = new Date(item.datetime);
        let date_str = chart.dateformat(date);
        
        return chart.tooltip
          .style("top", (event.pageY)+"px")
          .style("left",(event.pageX)+"px")
          .html(`<p><b>Fase</b> ${item.phase}</p>
                            <p><b>Fecha</b> ${date_str}</p>
                            <p><b>Distancia</b> ${item.excavated_distance}[m]</p>`)
        
      }
      component.selectAll(".circle")
      .data(newData.dataset)
      .join("circle")
      .attr("r", 3)
      .attr("phase", (item)=>item.phase)
      .attr("date", (item)=>item.datetime)
      .attr("value", (item)=>item.excavation_distance)
      .attr("fill",(item)=>{
          return color(item.phase);} )
      .attr("cx", (item)=>{
        return chart.xAxis(new Date(item.datetime));
      })
      .attr("cy", (item)=>{
        return chart.yAxis(item.excavated_distance);
        })
      .on("mouseover", () => {
        return chart.tooltip.style("visibility","visible")})
      .on("mousemove", mousemove)
      .on("mouseout", function(){return chart.tooltip.style("visibility", "hidden");});
    };


    
  };


  return chart;
}


export {ExcavationProgress};
